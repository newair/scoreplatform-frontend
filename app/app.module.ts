import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent }   from './app.component';
import { HttpModule }    from '@angular/http';

import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDbServiceScorePF }  from './inMemoryData.service';

import { AppRoutingModule }     from './app-routing.module';
import { TopScoreTable } from './topScoreTable.component';
import { PlayerService } from './player.service';
import { MenuComponent } from './menu.component';


@NgModule({
  imports:      [ BrowserModule,AppRoutingModule,HttpModule ],
  declarations: [ AppComponent,TopScoreTable,MenuComponent ],
  providers:[PlayerService],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }

