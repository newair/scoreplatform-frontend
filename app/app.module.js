"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var platform_browser_1 = require('@angular/platform-browser');
var app_component_1 = require('./app.component');
var http_1 = require('@angular/http');
var in_memory_web_api_module_1 = require('angular-in-memory-web-api/in-memory-web-api.module');
var inMemoryData_service_1 = require('./inMemoryData.service');
var app_routing_module_1 = require('./app-routing.module');
var topScoreTable_component_1 = require('./topScoreTable.component');
var player_service_1 = require('./player.service');
var menu_component_1 = require('./menu.component');
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [platform_browser_1.BrowserModule, app_routing_module_1.AppRoutingModule, http_1.HttpModule, in_memory_web_api_module_1.InMemoryWebApiModule.forRoot(inMemoryData_service_1.InMemoryDbServiceScorePF)],
            declarations: [app_component_1.AppComponent, topScoreTable_component_1.TopScoreTable, menu_component_1.MenuComponent],
            providers: [player_service_1.PlayerService],
            bootstrap: [app_component_1.AppComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map