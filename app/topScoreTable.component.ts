import {Component,OnInit  } from '@angular/core';
import { Player } from './player';
import { PlayerService } from './player.service';

@Component({

selector:'topscoretable',
moduleId:module.id,
templateUrl:'topScoreTable.component.html',
styleUrls:['topScoreTable.component.css']

})

export class TopScoreTable implements OnInit{
    players:Player[];
    constructor(private playerService:PlayerService) {
        
    }
    ngOnInit() :void{
        this.playerService.getPlayers().then(players=>
        this.players=players
        );
    }
}