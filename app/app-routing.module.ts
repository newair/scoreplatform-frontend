import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';//TopScoreTable
import { TopScoreTable } from './topScoreTable.component';


const routes:Routes=[
{path:'',redirectTo:'/topscore',pathMatch:'full'},
{path:'topscore',component:TopScoreTable}
]

/*

<script src="https://www.gstatic.com/firebasejs/3.5.0/firebase.js"></script>
<script>
  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyAUvX7x-MOjXEdiaszoGABt7o7BgdmRieo",
    authDomain: "puppy-on-the-road-score-board.firebaseapp.com",
    databaseURL: "https://puppy-on-the-road-score-board.firebaseio.com",
    storageBucket: "",
    messagingSenderId: "1074700734370"
  };
  firebase.initializeApp(config);
</script>*/

@NgModule({
    imports:[RouterModule.forRoot(routes)],
    exports:[RouterModule]
})

export class AppRoutingModule{}