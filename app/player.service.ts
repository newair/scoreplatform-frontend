import { Injectable } from '@angular/core';
import { Headers,Http } from '@angular/http';
import { Player } from './player';
import 'rxjs/add/operator/toPromise';


@Injectable()
export class PlayerService {

constructor(private http:Http) 
{ }

getPlayers():Promise<Player[]>{
    console.log('getting players');
 return this.http.get("http://localhost:3014/players",{headers: this.getHeaders()})
            .toPromise()
            .then(response=>
            response.json() as Player[])
            .catch(error=>
            console.log(error));
}

private getHeaders(){
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    return headers;
  }

}