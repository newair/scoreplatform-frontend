"use strict";
var InMemoryDbServiceScorePF = (function () {
    function InMemoryDbServiceScorePF() {
    }
    InMemoryDbServiceScorePF.prototype.createDb = function () {
        var players = [
            { id: 1, name: 'amila', score: 50 },
            { id: 2, name: 'ishara', score: 60 },
            { id: 2, name: 'ranatunga', score: 70 },
        ];
        return { players: players };
    };
    return InMemoryDbServiceScorePF;
}());
exports.InMemoryDbServiceScorePF = InMemoryDbServiceScorePF;
//# sourceMappingURL=inMemoryData.service.js.map