import { Component } from '@angular/core';
@Component({
  selector: 'my-app',
  template: `
  <menue-bar></menue-bar>
  <router-outlet></router-outlet>
  `
})
export class AppComponent { }
